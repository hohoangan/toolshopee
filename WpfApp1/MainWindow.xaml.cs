﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Diagnostics;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string folderPath = "";
        public MainWindow()
        {
            InitializeComponent();

            DateTime dt = DateTime.Now;
            DateTime dtEnd = new DateTime(2021, 1, 1);
            if(dt > dtEnd)
            {
                MessageBox.Show("Lien he An");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.InitialDirectory = "C:\\Users";
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                //clear
                txtName.Document.Blocks.Clear();
                stackPanelImg.Children.Clear();
                folderPath = dialog.FileName;
                txtFolderName.Text = folderPath;
            }
            return;
        }

        void ChangeDetailAll()
        {
            if (string.Empty.Equals(txtSubject.Text))
            {
                MessageBox.Show("Hãy nhập subject cho sản phẩm");
                txtSubject.Focus();
                return;
            }
            if (string.Empty.Equals(txtTag.Text))
            {
                MessageBox.Show("Hãy nhập #Tag cho sản phẩm");
                txtSubject.Focus();
                return;
            }
            if (string.Empty.Equals(folderPath))
            {
                MessageBox.Show("Chọn folder chứa hình sản phẩm");
                return;
            }
            
            foreach (string filenameJpg in Directory.EnumerateFiles(folderPath, "*.jpg"))
            {
                try
                {
                    txtName.AppendText("\n");
                    txtName.AppendText(System.IO.Path.GetFileName(filenameJpg));
                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri(filenameJpg));
                    image.Width = 80;
                    image.Height = 80;
                    stackPanelImg.Children.Add(image);
                    string folderCopy = System.IO.Path.Combine(folderPath, "Copy");
                    // Determine whether the directory exists.
                    if (!Directory.Exists(folderCopy))
                    {
                        // Try to create the directory.
                        DirectoryInfo di = Directory.CreateDirectory(folderCopy);
                    }
                    string filename = System.IO.Path.Combine(folderCopy, System.IO.Path.GetFileName(filenameJpg));
                    //tag
                    List<string> tag = txtTag.Text.Trim().Split(',').ToList();
                    ChangeDetail(filenameJpg, filename, txtSubject.Text, tag);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message +"\n"+ filenameJpg);
                }
            }
            MessageBoxResult rsOK = MessageBox.Show("Đã tạo file thành công!\nCó muốn mở file vừa xuất?", "", MessageBoxButton.OKCancel);
            if (rsOK.Equals(MessageBoxResult.OK))
            {
                CommonOpenFileDialog dialog = new CommonOpenFileDialog();
                dialog.InitialDirectory = folderPath;
                dialog.ShowDialog();
            }
        }

        void ChangeDetail(string filename, string newfilename, string subject, List<string> tag)
        {
            // Open a Stream and decode a JPEG image
            FileStream stream3 = new FileStream(newfilename, FileMode.Create);
            FileStream imageStreamSource = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);

            try
            {
                BitmapMetadata myBitmapMetadata = new BitmapMetadata("tiff");//tiff
                TiffBitmapEncoder encoder3 = new TiffBitmapEncoder();
                myBitmapMetadata.ApplicationName = "Ant Store https://shopee.vn/";
                myBitmapMetadata.Author = new ReadOnlyCollection<string>(
                    new List<string>() { "Ant Store" });
                myBitmapMetadata.CameraManufacturer = "Ant Store";
                myBitmapMetadata.CameraModel = "Ant2020";
                myBitmapMetadata.Comment = "Nice Picture";
                myBitmapMetadata.Copyright = DateTime.Now.Year.ToString();
                myBitmapMetadata.DateTaken = DateTime.Now.ToString();
                myBitmapMetadata.Keywords = new ReadOnlyCollection<string>(
                    //new List<string>() { "#AntStore", "#Ant" }
                    tag
                    );
                myBitmapMetadata.Rating = 5;
                myBitmapMetadata.Subject = subject;
                myBitmapMetadata.Title = subject;

                // Create a new frame that is identical to the one 
                // from the original image, except for the new metadata. 
                JpegBitmapDecoder decoder = new JpegBitmapDecoder(imageStreamSource, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                BitmapSource bitmapSource = decoder.Frames[0];

                encoder3.Frames.Add(
                    BitmapFrame.Create(
                    bitmapSource,
                    decoder.Frames[0].Thumbnail,
                    myBitmapMetadata,
                    decoder.Frames[0].ColorContexts));

                encoder3.Save(stream3);
                stream3.Close();
                imageStreamSource.Close();
            }
            catch (Exception ex)
            {
                stream3.Close();
                imageStreamSource.Close();
                MessageBox.Show(ex.Message);
            }
        }
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            ChangeDetailAll();
        }
    }
}
